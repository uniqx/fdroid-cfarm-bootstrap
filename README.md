# F-Droid C-Farm Bootstrap

This project automates the setup of some of our (OSUOSL donated) GCC-build-farm machines.

## Basic setup

All automated machines use `provision.yml` as basic setup. This entails:

* llvm partition layout
* nesting ready libvirt-vagrant setup
* some cli tools we like/need

execute playbook:

    ansible-galaxy install -f -r requirements.yml -p .galaxy
    ansible-playbook -i xxx.osuosl.org, provision.yml

## Individual machine setup

Individual services on individual hosts are scripted in specific playbooks:

### contributor instances

Deployments for contributor VMs is scripted in `contributorVMsXXX.yml`. These
scripts install and configure
[fdroid-bootstarp-buildserver](https://gitlab.com/fdroid/fdroid-bootstrap-buildserver)
to `/srv/<instancename>`.

#### provisioning a contributor vm

1. run contributor vm playbook, eg: `ansible-playbook -i xxx.osuosl.org, contributorVMs148.yml`
2. connect to server, eg: `ssh xxx.osuosl.org`
3. start contributor vm: `cd /srv/<vm-name> && vagrant up`
4. run boostrap wrapper script, eg: `cd /srv/<vm-name> && vagrant ssh -c 'sudo -i /root/bootstrap-buildserver'`
