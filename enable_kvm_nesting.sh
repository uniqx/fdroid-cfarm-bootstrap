#! /bin/bash

set -e

_test_debian9() {
    if [ -z "`grep 'Debian GNU/Linux 9' /etc/issue`" ]; then
        echo "Error: This is not a Debian 9 based System. Can not enable KVM nesting."
        exit 1
    fi
}

_test_root() {
    if [ "$UID" != "0" ]; then
        echo "Error: This script requires root privileges. Please run as root."
        exit 1
    fi
}

_grep_cpuinfo_vmx() {
    [ -n "`grep 'flags\s*:.*\svmx\s' /proc/cpuinfo`" ]
}

_grep_cpuinfo_svm() {
    [ -n "`grep 'flags\s*:.*\ssvm\s' /proc/cpuinfo`" ]
}


_kernel_module_in_use() {
    if _mod_enabled "$1"; then
        USE="`lsmod | grep \"^$1\" | awk '{print $3}'`"
        [ "$USE" != "0" ]
    else
        false
    fi
}

_mod_enabled() {
    [ -n "`lsmod | grep '^$1'`" ]
}

_nesting_enabled() {
    if [ -f /sys/module/"$1"/parameters/nested ]; then
        NESTED=`cat /sys/module/"$1"/parameters/nested`
        [ "$NESTED" = "Y" -o "$NESTED" = "1" ]
    else
        false
    fi
}

_human_readable() {
    if [ "$1" = "kvm_intel" ]; then
        echo 'Intel VT-x'
    elif [ "$1" = "kvm_amd" ]; then
        echo 'AMD SVM'
    fi
}

_enable_kvm_nesting() {

    MODULE="$1"

    if _nesting_enabled "$MODULE"; then
        echo "Nested KVM support for `_human_readable $MODULE` is already enabled."
        exit 0
    else
        if _kernel_module_in_use $MODULE; then
            echo "Error: `_human_readable $MODULE` virutalization is currently in use."
            echo "(If you've got VMs running, you'll need to shut them down.)"
            exit 1
        fi

        if [ "$MODULE" = "kvm_amd" ]; then
            echo "options kvm-amd nested=1" > /etc/modprobe.d/kvm-amd.conf
        elif [ "$MODULE" = "kvm_intel" ]; then
            echo "options kvm-intel nested=1" > /etc/modprobe.d/kvm-intel.conf
        fi

        rmmod $MODULE
        modprobe $MODULE

        if _nesting_enabled "$MODULE"; then
            echo "Sucessfully enabled KVM nesting support for `_human_readable $MODULE`."
        else
            echo "Error: Failed to enable KVM nesting support for `_human_readable $MODULE`"
        fi
    fi
    
}


_test_debian9
_test_root
if _grep_cpuinfo_svm; then
    _enable_kvm_nesting "kvm_amd"
elif _grep_cpuinfo_vmx; then 
    _enable_kvm_nesting "kvm_intel"
else
    echo "Error: Could detect neiter AMD SVM nor Intel VT-x support."
    echo "(You might need to enable virtualization in BIOS/UEFI.)"
    exit 1
fi
